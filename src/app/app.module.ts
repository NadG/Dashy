import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LOCALE_ID, NgModule} from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeIt from '@angular/common/locales/it';
import {HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import { CardComponent } from './commons/card/card.component';
import { HomeComponent } from './main/home/home.component';
import { LolComponent } from './components/lol/lol.component';
import {ForecastsWidgetModule} from './components/forecasts/forecasts-widget.module';
import { NasaApodComponent } from './components/nasa-apod/nasa-apod.component';
import {TwitchComponent} from './components/twitch/twitch.component';
import { SteamComponent } from './components/steam/steam.component';
import { HeaderComponent } from './main/header/header.component';
import { GitInfoComponent } from './components/git-info/git-info.component';
import { CountdownComponent } from './components/countdown/countdown.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule, MatNativeDateModule} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material';
import { TableComponent } from './commons/table/table.component';
import { FormsModule } from '@angular/forms';
import { ButtonGroupComponent } from './commons/button/button-group/button-group.component';
import { ButtonComponent } from './commons/button/button/button.component';
import {MatDialogModule} from '@angular/material/dialog';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {CardHeaderComponent} from './commons/card-header/card-header.component';
import { EventDialogComponent } from './components/countdown/event-dialog/event-dialog.component';
import {LayoutDirective} from './commons/directives/layout.directive';
import { MotivatorComponent } from './components/motivator/motivator.component';

registerLocaleData(localeIt, 'it');
@NgModule({
	declarations: [
		AppComponent,
		CardComponent,
		HomeComponent,
		LolComponent,
		NasaApodComponent,
		TwitchComponent,
		SteamComponent,
		HeaderComponent,
		GitInfoComponent,
		CountdownComponent,
		TableComponent,
		ButtonGroupComponent,
		ButtonComponent,
		CardHeaderComponent,
		EventDialogComponent,
		LayoutDirective,
		MotivatorComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		HttpClientModule,
		ReactiveFormsModule,
		ForecastsWidgetModule,
		FormsModule,
		MatDialogModule,
		DragDropModule
		// MatInputModule,
		// MatDatepickerModule,
		// MatFormFieldModule,
		// MatDatepickerModule,
		// MatNativeDateModule
	],
	providers: [
		// MatDatepickerModule,
		{ provide: LOCALE_ID, useValue: 'it' }
	],
	bootstrap: [AppComponent],
	entryComponents: [EventDialogComponent]
})
export class AppModule {
}
