import {Component, OnInit} from '@angular/core';
import {RitoService} from '../../components/lol/rito.service';
import {elementAt} from 'rxjs/operators';
import {NasaService} from '../../components/nasa-apod/nasa.service';

@Component({
	selector: 'dashy-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	constructor(private dts: RitoService, ) {
	}

	ngOnInit() {

	}

}
