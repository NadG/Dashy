import {Component, Input, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {debounceTime, delay} from 'rxjs/operators';
import {WeatherDataService} from '../weather-data.service';
import {WeatherInfoInterface} from '../weather-info';
import {WeatherUtilitiesService} from '../weather-utilities.service';

@Component({
	selector: 'dashy-forecasts',
	templateUrl: './forecasts.component.html',
	styleUrls: ['./forecasts.component.scss'],
	animations: [
		trigger('anim', [
			state('in', style({transform: 'translateX(0)'})),
			transition('void => *', [
				style({transition: 'ease-in', opacity: 0}),
				animate('1s')
			]),
			transition('* => void', [
				animate('1s', style({transition: 'ease-out', opacity: 1}))
			])
		])
	]
})
export class ForecastsComponent implements OnInit {
	private currentForecastsData;
	forecasts: FormGroup;
	@Input() public city: string;
	public citySelected: string;
	public error: string;
	public windy = {}

	constructor(
		private weatherDataService: WeatherDataService,
		private utilitiesService: WeatherUtilitiesService,
		private fb: FormBuilder
	) {
		console.log('this.constructor.name ' + this.constructor.name);
	}

	ngOnInit() {
		this.forecasts = this.fb.group({
			city: ['', [Validators.required, Validators.pattern('^[A-Za-z]+(?:[ -][A-Za-z]+)*$')]]
		});
		this.onChanges();
	}

	onChanges(): void {
		this.forecasts.get('city').valueChanges
			.pipe(debounceTime(1000))
			.subscribe(val => this.getWeatherForecasts());
	}

	getWeatherForecasts(): void {
		this.citySelected = this.forecasts.get('city').value;
		this.weatherDataService.storeCityValue(this.citySelected);

		this.weatherDataService.getCurrentForecast(this.forecasts.get('city').value)
			.pipe(
				delay(1000),
			)
			.subscribe((res: WeatherInfoInterface) => {
				this.error = '';
				this.currentForecastsData = res;
				console.log(res);
				this.buildWidget();
			}, error => {
				error.status === 404 ? this.error = error.error.message : console.log(error);
			});
	}

	buildWidget() {
		Object.assign(this.windy, {
			mainC: this.utilitiesService.kelvinToCelsius(this.currentForecastsData.main.temp),
			maxC: this.utilitiesService.kelvinToCelsius(this.currentForecastsData.main.temp_max),
			minC: this.utilitiesService.kelvinToCelsius(this.currentForecastsData.main.temp_min),
			humidity: this.currentForecastsData.main.humidity
		})
		this.utilitiesService.getDateTime(this.currentForecastsData.dt);
		this.getWeatherGenerals();
		this.getWeatherDescription();
		this.getSunInfo();
	}

	getWeatherGenerals() {
		this.currentForecastsData.weather.map(e => {
			return Object.assign(this.windy, {
				weatherID : e.id,
				weatherMessage : e.description
			})
		});
	}

	getWeatherDescription() {
		this.currentForecastsData.weather.map(e => {
			return Object.assign(this.windy, {
				description: e.description
			})
		});
	}

	getSunInfo() {
		const sr = this.currentForecastsData.sys.sunrise;
		const ss = this.currentForecastsData.sys.sunset;
		Object.assign(this.windy, {
			sunrise : this.utilitiesService.getCurrTime(this.utilitiesService.getDateTime(sr)),
			sunset : this.utilitiesService.getCurrTime(this.utilitiesService.getDateTime(ss))
		})
	}

}
