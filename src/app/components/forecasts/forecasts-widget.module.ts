import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {NextForecastsComponent} from './next-forecasts/next-forecasts.component';
import {ForecastsComponent} from './forecast-widget-container/forecasts.component';
import {WeatherDataService} from './weather-data.service';

@NgModule({
	imports: [
		CommonModule,
		ReactiveFormsModule
	],
	declarations: [
		NextForecastsComponent,
		ForecastsComponent
	],
	providers: [
		WeatherDataService
	],
	exports: [
		ForecastsComponent,
		NextForecastsComponent
	]
})
export class ForecastsWidgetModule {
}
