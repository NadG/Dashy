export interface WeatherInfoInterface {
	base: string;
	clouds: object;
	cod: number;
	coord: object;
	dt: number;
	id: number;
	main: object;
	name: string;
	// sys: object;
	sys: number;
	visibility: number;
	weather: Array<Object>;
}
	// date: string;
	// time: string;
	// humidity: number;
	// weatherID: number;
	// weatherMessage: string;
	// description: string;
	// mainC: number;
	// minC: number;
	// maxC: number;
	// sunrise: string;
	// sunset: string;
	// error: string;
