import { Component, OnInit} from '@angular/core';
import {WeatherDataService} from '../weather-data.service';
import {WeatherUtilitiesService} from '../weather-utilities.service';
import {delay} from 'rxjs/operators';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
	selector: 'dashy-next-forecasts',
	templateUrl: './next-forecasts.component.html',
	styleUrls: ['./next-forecasts.component.scss'],
	animations: [
		trigger('anim', [
			state('in', style({transform: 'translateX(0)'})),
			transition('void => *', [
				style({transition: 'ease-in', opacity: 0}),
				animate('1s')
			]),
			transition('* => void', [
				animate('1s', style({transition: 'ease-out', opacity: 1}))
			])
		])
	]
})
export class NextForecastsComponent implements OnInit {
	public citySelected: string;
	private listForecasts: any;
	infoForecasts;

	constructor(
		private weatherDataService: WeatherDataService,
		private utilitiesService: WeatherUtilitiesService
	) {
		console.log('this.constructor.name ' + this.constructor.name);
		// Chiamata al weatherDataService per la città
		this.weatherDataService.storeCityValue$.subscribe( (city: string) => {
			this.citySelected = city;
			// Chiamata al weatherDataService per il meteo
			this.weatherDataService.getThreeHoursForecast(this.citySelected)
				.pipe(
					delay( 1000 ),
				)
				.subscribe( (res: any ) => {
					this.listForecasts = res.list.slice(0, 4);
					console.log( this.listForecasts );
					this.buildWidget();
				});
		});
	}

	ngOnInit() {}

	buildWidget() {
		const ser = this.utilitiesService;
		this.infoForecasts = this.listForecasts.reduce(
			(acc, cur) => [
				...acc,
				{
					id: cur.weather[0].id,
					time: ser.getCurrTime(
						ser.getDateTime(cur.dt)
					),
					temp: ser.kelvinToCelsius(cur.main.temp),
					isNight: ser.isNight(ser.getCurrTime(
						ser.getDateTime(cur.dt)
					))
				}
			],
			[]
		);
		console.log( this.infoForecasts );
	}

}
