import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NextForecastsComponent } from './next-forecasts.component';

describe('NextForecastsComponent', () => {
  let component: NextForecastsComponent;
  let fixture: ComponentFixture<NextForecastsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NextForecastsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NextForecastsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
