import {Injectable} from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class WeatherUtilitiesService {

	constructor() {}

	getCurrTime(currDateTime) {
		return currDateTime.slice(11, 17);
	}

	getCurrDate(currDateTime) {
		return currDateTime.slice(0, 10);
	}

	getDateTime(dateToTransform) {
		let currDateTime = new Date(dateToTransform * 1000).toLocaleString();
		this.getCurrDate(currDateTime);
		this.getCurrTime(currDateTime);
		return currDateTime;
	}

	kelvinToCelsius(kelvin) {
		return kelvin - 273.15;
	}

	isNight(time) {
		let j = time.slice( 0, 2 );
		if (j >= 19 && j <= 23 || j === 0 || j >= 1 && j <= 5) {
			return true;
		}
	}

}
