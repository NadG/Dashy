import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
	providedIn: 'root'
})
// https://github.com/unsplash/unsplash-js/pull/63 Ci sono dei problemi i cui fix non sono previsti nel breve periodo.
// Ti tocca tenere sott'occhio le api e continuare ad usare il fetch da css

export class UnsplashService {
	private URL = 'https://api.unsplash.com/';
	private username = 'nadg';
	private client_id = 'a554093c4592d2cbd0da6d9f21e39de660cf748064bba0e6b1a0750628544ebf';
	private headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
	// client_id=YOUR_ACCESS_KEY
	// / header Accept-Version: v1
	constructor(private http: HttpClient) {
	}

	//	 GET /users/:username/collections
	getUserCollection(): Observable<any> {
		// return this.http.get(`${this.URL}/photos/${this.username}/collections`);
		return this.http.get('https://api.unsplash.com/users/fableandfolk/collections');
	}
}
