
import {from as observableFrom, Observable, Subject} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';


@Injectable({
	providedIn: 'root'
})

export class WeatherDataService {
	private API_KEY = 'ae43ab0a9028a25d791ca6eff018161b';
	private URL = 'https://api.openweathermap.org/data/2.5/';
	private THREE_HOUR_FORECAST = 'forecast';
	private CURRENT_FORECAST = 'weather';
	private headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});

	constructor( private http: HttpClient) {
		console.log('this.constructor.name ' + this.constructor.name);
		this.storeCityValue$ = this.storeCityValueSubject.asObservable();
	}

	storeCityValue$: Observable<any>;
	private storeCityValueSubject = new Subject<any>();

	storeCityValue(city) {
		console.log(city);
		this.storeCityValueSubject.next(city);
	}

	// getCurrentForecast(city) {
	getCurrentForecast(city): Observable<any> {
		return observableFrom(
			this.http.get<any>(`${this.URL}${this.CURRENT_FORECAST}?q=${city}&APPID=${this.API_KEY}`)
		);
	}

	getThreeHoursForecast(city): Observable<any> {
		return observableFrom(
			this.http.get<any>(`${this.URL}${this.THREE_HOUR_FORECAST}?q=${city}&APPID=${this.API_KEY}`)
		);
	}
}


