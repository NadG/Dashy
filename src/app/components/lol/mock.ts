// // 8.17.1
// export const CHAMPIONS: any = [
// 	{
// 		id: 37,
// 		name: 'Sona',
// 		role: 'support',
// 		secondary_role: 'mid'
// 	},
// 	{
// 		id: 21,
// 		name: 'Miss Fortune',
// 		role: 'adc',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 117,
// 		name: 'Lulu',
// 		role: 'support',
// 		secondary_role: 'mid'
// 	},
// 	{
// 		id: 40,
// 		name: 'Janna',
// 		role: 'support',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 497,
// 		name: 'Rakan',
// 		role: 'support',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 25,
// 		name: 'Morgana',
// 		role: 'support',
// 		secondary_role: 'mid'
// 	},
// 	{
// 		id: 222,
// 		name: 'Jinx',
// 		role: 'adc',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 202,
// 		name: 'Jhin',
// 		role: 'adc',
// 		secondary_role: 'mid'
// 	},
// 	{
// 		id: 44,
// 		name: 'Taric',
// 		role: 'support',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 498,
// 		name: 'Xayah',
// 		role: 'adc',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 145,
// 		name: 'Kaisa',
// 		role: 'adc',
// 		secondary_role: 'mid'
// 	},
// 	{
// 		id: 267,
// 		name: 'Nami',
// 		role: 'support',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 111,
// 		name: 'Nautilus',
// 		role: 'support',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 56,
// 		name: 'Nocturne',
// 		role: 'jungle',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 61,
// 		name: 'Orianna',
// 		role: 'mid',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 78,
// 		name: 'Poppy',
// 		role: 'top',
// 		secondary_role: 'support'
// 	},
// 	{
// 		id: 555,
// 		name: 'Pyke',
// 		role: 'support',
// 		secondary_role: 'jungle'
// 	},
// 	{
// 		id: 421,
// 		name: 'RekSai',
// 		role: 'jungle',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 35,
// 		name: 'Shaco',
// 		role: 'jungle',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 17,
// 		name: 'Teemo',
// 		role: 'top',
// 		secondary_role: 'adc'
// 	},
// 	{
// 		id: 412,
// 		name: 'Thresh',
// 		role: 'support',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 12,
// 		name: 'Tristana',
// 		role: 'adc',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 110,
// 		name: 'Varus',
// 		role: 'adc',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 67,
// 		name: 'Vayne',
// 		role: 'adc',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 45,
// 		name: 'Veigar',
// 		role: 'mid',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 161,
// 		name: 'Velkoz',
// 		role: 'mid',
// 		secondary_role: 'support'
// 	},
// 	{
// 		id: 115,
// 		name: 'Ziggs',
// 		role: 'mid',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 26,
// 		name: 'Zilean',
// 		role: 'support',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 142,
// 		name: 'Zoe',
// 		role: 'mid',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 266,
// 		name: 'Aatrox',
// 		role: 'top',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 103 ,
// 		name: 'Ahri' ,
// 		role: 'mid',
// 		secondary_role: ''
// 	},{
// 		id: 84,
// 		name: 'Akali' ,
// 		role:  'mid',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 12,
// 		name: 'Alistar',
// 		role: 'support' ,
// 		secondary_role: ''
// 	},
// 	{
// 		id: 32,
// 		name: 'Amumu',
// 		role: 'jungle' ,
// 		secondary_role: ''
// 	},
// 	{
// 		id: 34,
// 		name: 'Anivia',
// 		role: 'mid',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 1,
// 		name: 'Annie' ,
// 		role: 'mid',
// 		secondary_role: 'support'
// 	},
// 	{
// 		id: 22,
// 		name:  'Ashe',
// 		role: 'adc' ,
// 		secondary_role: ''
// 	},
// 	{
// 		id: 136 ,
// 		name: 'Aurelion Sol',
// 		role: ,
// 		secondary_role: ''
// 	},
// 	{
// 		id: 268,
// 		name: 'Azir',
// 		role: 'mid',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 432,
// 		name: 'Bard',
// 		role: 'support',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 53 ,
// 		name: "Blitzcrank",
// 		role: 'support' ,
// 		secondary_role: ''
// 	},
// 	{
// 		id: 63,
// 		name: "Brand" ,
// 		role: 'mid',
// 		secondary_role: 'support'
// 	},
// 	{
// 		id: 201,
// 		name: '"Braum"' ,
// 		role: 'support' ,
// 		secondary_role: ''
// 	},
// 	{
// 		id: 51,
// 		name:"Caitlyn" ,
// 		role: 'adc',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 164,
// 		name: "Camille",
// 		role: 'top',
// 		secondary_role: 'jungle'
// 	},
// 	{
// 		id: 69 ,
// 		name: "Cassiopeia",
// 		role: 'mid',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 31 ,
// 		name: "Chogath",
// 		role: 'top',
// 		secondary_role: 'mid'
// 	},
// 	{
// 		id: 42 ,
// 		name: "Corki",
// 		role:'mid' ,
// 		secondary_role: ''
// 	},
// 	{
// 		id: 122,
// 		name: "Darius",
// 		role: 'top' ,
// 		secondary_role: ''
// 	},
// 	{
// 		id:131 ,
// 		name: "Diana",
// 		role: 'mid',
// 		secondary_role: 'jungle'
// 	},
// 	{
// 		id: 119 ,
// 		name: "Draven",
// 		role: 'adc',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 36,
// 		name: "DrMundo",
// 		role:'top' ,
// 		secondary_role: 'jungle'
// 	},
// 	{
// 		id: 245,
// 		name: "Ekko",
// 		role: 'mid',
// 		secondary_role: 'jungle'
// 	},
// 	{
// 		id: 60,
// 		name: "Elise",
// 		role: 'jungle',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 28 ,
// 		name: "Evelynn",
// 		role: 'junlge',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 81,
// 		name: "Ezreal",
// 		role: 'adc',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 9,
// 		name: "Fiddlesticks",
// 		role: 'support',
// 		secondary_role: 'junlge'
// 	},
// 	{
// 		id: 114 ,
// 		name: "Fiora",
// 		role: 'top',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 105 ,
// 		name: "Fizz",
// 		role: 'mid',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 3 ,
// 		name: "Galio",
// 		role: 'mid',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 41,
// 		name: "Gangplank",
// 		role: 'top',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 86 ,
// 		name: "Garen",
// 		role: 'top',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 150 ,
// 		name: "Gnar",
// 		role: 'top',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 79,
// 		name: "Gragas",
// 		role: 'jungle',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 104 ,
// 		name: "Graves",
// 		role: 'jungle',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 120,
// 		name: "Hecarim",
// 		role: 'jungle',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 74,
// 		name: "Heimerdinger",
// 		role: 'mid',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 420,
// 		name: "Illaoi",
// 		role: 'top',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 39,
// 		name: "Irelia",
// 		role: 'top',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 427,
// 		name: "Ivern",
// 		role: 'jungle',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 59,
// 		name: "JarvanIV",
// 		role: 'jungle',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 24 ,
// 		name: "Jax",
// 		role: 'jungle',
// 		secondary_role: 'top'
// 	},
// 	{
// 		id: 126,
// 		name: "Jayce",
// 		role: 'top',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 429,
// 		name: "Kalista",
// 		role: 'adc',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 43,
// 		name: "Karma",
// 		role: 'support',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 38 ,
// 		name: "Kassadin",
// 		role: 'mid' ,
// 		secondary_role: ''
// 	},
// 	{
// 		id: 55 ,
// 		name: "Katarina",
// 		role: 'mid',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 10 ,
// 		name: "Kayle",
// 		role: 'top',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 141,
// 		name: "Kayn",
// 		role: 'jungle',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 85 ,
// 		name: "Kennen",
// 		role: 'top',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 121,
// 		name: "Khazix",
// 		role: 'jungle',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 203 ,
// 		name: "Kindred",
// 		role: 'jungle',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 240 ,
// 		name: "Kled",
// 		role: 'top',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 96,
// 		name: "KogMaw",
// 		role: 'adc',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 7,
// 		name: "Leblanc",
// 		role: 'mid',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 64 ,
// 		name:"LeeSin" ,
// 		role: 'jungle',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 89 ,
// 		name: "Leona",
// 		role: 'support',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 127,
// 		name: "Lissandra",
// 		role: 'mid',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 236 ,
// 		name: "Lucian",
// 		role: 'adc',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 99,
// 		name: "Lux",
// 		role: 'mid',
// 		secondary_role: 'support'
// 	},
// 	{
// 		id: 54,
// 		name: "Malphite",
// 		role: 'top',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 90,
// 		name: "Malzahar",
// 		role: 'mid',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 57,
// 		name: "Maokai",
// 		role: 'top',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 11,
// 		name: "MasterYi",
// 		role: 'jungle',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 62,
// 		name: 'Wukong',
// 		role: 'top',
// 		secondary_role: ''
// 	},
// 	{
// 		id: 82,
// 		name: "Mordekaiser",
// 		role: 'top',
// 		secondary_role: ''
// 	},
// 	{
// 		id: ,
// 		name: ,
// 		role: ,
// 		secondary_role: ''
// 	},
//
//
// ]
//
//
//
//
// /*
// * */
