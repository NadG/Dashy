import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import 'rxjs/Rx';
// import {CHAMPIONS} from './mock';

@Injectable({
	providedIn: 'root'
})
export class RitoService {
	private ritoUrl = 'https://euw1.api.riotgames.com/lol/';
	private cggUrl = 'http://api.champion.gg/v2/';

	private riot_API_KEY = 'RGAPI-84448fab-0e14-4a44-a0b3-1e727c6700e1';
	private championGG_KEY = '4f1d6ff0b52fe2fa4ad3798b846bb5bc';

	private summID = '81374139';
	private elo = 'SILVER';
	private limit = '100';
	private summonerAPI = 'summoner/v3/summoners';
	private championMasteryAPI = 'champion-mastery/v3/champion-masteries';
	private champInfo = `champions/1?elo=${this.elo}&api_key=${this.championGG_KEY}`

	constructor(private http: HttpClient) {
	}

	// getChampStaticInfo(): Observable<any> {
	// 	return Observable.of(CHAMPIONS).delay(0);
	// }

	getChampInfo() {
		return this.http.get('http://ddragon.leagueoflegends.com/cdn/8.20.1/data/it_IT/champion.json');
	}

	getOverallPerformance() {
		return this.http.get (`${this.cggUrl}overall?elo=${this.elo}&api_key=${this.championGG_KEY}`);
	}

}
// 	getSummonerByID() {
// 		return this.http.get(`${this.ritoUrl}${this.summonerAPI}/${this.summID}?api_key=${this.riot_API_KEY}`);
// 	}
// 	// Requests overall performance champ data sets (as in championgg landing page)
//
//
// }
// https://euw1.api.riotgames.com/lol/summoner/v3/summoners/81374139?api_key=RGAPI-3c5ec14e-02e1-4ab1-8b60-f6f92c1019a5
