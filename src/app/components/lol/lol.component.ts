import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {RitoService} from './rito.service';
import {catchError, delay, elementAt, pluck, take} from 'rxjs/operators';
import {throwError} from 'rxjs';


@Component({
	selector: 'dashy-lol',
	templateUrl: './lol.component.html',
	styleUrls: ['./lol.component.scss']
})
export class LolComponent implements OnInit, AfterViewInit {
	imgUrl = `http://ddragon.leagueoflegends.com/cdn/8.20.1/img/champion/`;
	champions = [];
	bestChampions = []
	lastPatch;
	positions;
	rates = [];
	sprites = [];
	src

	constructor(
		private lolSer: RitoService
	){
		this.getLatestPatch()
	}

	ngOnInit() {

		this.lolSer.getChampInfo()
			.subscribe( (res:any) => {
				// console.log(res.data)
				let champions = res.data
				Object.entries(champions).forEach(e => {
					this.champions.push(e[1])
				})
			return this.champions
		})
	}

	ngAfterViewInit() {
	}

	getLatestPatch(){
		this.lolSer.getOverallPerformance()
			.subscribe( (res:any) => {
				let arr = res;
				 this.lastPatch = arr[0]
				this.getPositions()
			})
	}

	getPositions() {
		const {positions} = this.lastPatch;
		this.positions = positions;
		this.getBotWinrate()
	}


	getBotWinrate() {
		console.log( this.positions );
		const { DUO_CARRY: { winrate: { best: bestAdc } } } = this.positions;
		const { DUO_SUPPORT: { winrate: { best: bestSupp } } } = this.positions;

		this.rates.push({
			bestAdc: bestAdc,
			bestSupp: bestSupp,
		})
		return this.checkID();
	}

	checkID() {
		this.champions.forEach( e => {
			let ck = parseInt(e.key);
			this.rates.forEach( j => {
				if (ck === j.bestAdc.championId) {
					 Object.assign(j.bestAdc, {sprite: e.image.full})
					let {score: adcS, ...bestAdc} = j.bestAdc;
					return this.bestChampions.push({score: adcS, role: 'adc', ...bestAdc});

				} else if ( ck === j.bestSupp.championId) {

					Object.assign(j.bestSupp, {sprite: e.image.full})
					let { score: suppS, ...bestSupp} = j.bestSupp;
					return this.bestChampions.push({score: suppS, role: 'support', ...bestSupp});
				}
			})
		});
		console.log( this.bestChampions );
	}
}
