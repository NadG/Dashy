import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class NasaService {

	constructor(private http: HttpClient) {
	}

	getApod(): Observable<any[]> {
		return this.http.get<any>('https://api.nasa.gov/planetary/apod?api_key=VxSqb5xuiPIHzmV2f1zPbG1H9YhCEo4HCQIQxsR2');
	}
}
