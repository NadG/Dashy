import {Component, OnInit} from '@angular/core';
import {NasaService} from './nasa.service';

@Component({
	selector: 'dashy-nasa-apod',
	templateUrl: './nasa-apod.component.html',
	styleUrls: ['./nasa-apod.component.scss']
})
export class NasaApodComponent implements OnInit {
	copyright: string;
	date: string;
	description: string;
	url?: string;
	hdurl?: string;
	title: string;
	mediatype: string;
	constructor(private nasaS: NasaService) {
	}

	ngOnInit() {

		this.nasaS.getApod().subscribe( (res:any) => {
			this.mediatype = res.media_type;
			this.copyright = res.copyright;
			this.date = res.date;
			this.description = res.explanation;
			this.url = res.url;
			this.hdurl = res.hdurl;
			this.title = res.title;
			console.log( res );
		})
	}

}
