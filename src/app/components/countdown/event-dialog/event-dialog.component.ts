import {Component, Inject, OnInit, EventEmitter, Output} from '@angular/core';
import {Validators} from '@angular/forms';
import {FormBuilder} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';


const dialogSchema: Object = {
	buttons: [
		{
			name: 'Cancel',
			eventName: 'cancelEvent',
			disabled: false,
			rect: true
		},
		{
			name: 'Save',
			eventName: 'saveEvent',
			disabled: false,
			rect: true
		}
	]
};
@Component({
	selector: 'dashy-event-dialog',
	templateUrl: './event-dialog.component.html',
	styleUrls: ['./event-dialog.component.scss']
})
export class EventDialogComponent implements OnInit {
	dialogSchema = dialogSchema;
	today;
	todayFormatAccepted;
	dateSelected;
	msPD = 1000 * 60 * 60 * 24;
	dueDate;

	eventForm = this.fb.group({
		date: ['', Validators.required],
		description: ['', Validators.required],
	});

	constructor(private fb: FormBuilder,
			  private dialogRef: MatDialogRef<EventDialogComponent>,
			  @Inject(MAT_DIALOG_DATA) data) {
	}

	ngOnInit() {
		this.today = new Date().toLocaleDateString('it-IT');
		let p = this.today.toString().split('/');
		let d = p[0];
		let m = p[1];
		let y = p[2];
		this.eventForm.get('date').patchValue(y + '-' + m + '-' + d); //data salvata nel control
		this.todayFormatAccepted = this.eventForm.get('date').value;
		console.log(this.today);// data mostrata in input
	}

	change(ev) {
		console.log(ev.srcElement.value);
		this.dateSelected = ev.srcElement.value;
		this.getDiff();
	}

	getDiff() {
		let a = new Date(this.todayFormatAccepted); //oggi
		let b = new Date(this.dateSelected);//scadenza
		this.dateDiffInDays(a, b);
	}

	dateDiffInDays(a, b) {
		const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
		const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
		this.dueDate = Math.floor((utc2 - utc1) / this.msPD);
		return Math.floor((utc2 - utc1) / this.msPD);
	}

	onSubmit() {
		Object.assign(this.eventForm.value, {scadenza: this.dueDate})
		this.dialogRef.close(this.eventForm.value);
	}

	close() {
		this.dialogRef.close();
	}

}
