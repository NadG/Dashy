import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {EventDialogComponent} from './event-dialog/event-dialog.component';

const eventSource: Array<Object> = [
	{nome: 'Netflix', giorni: '- 34 giorni'}
];

const eventTableSchema: Object = {
	displayedColumns: [
		'nome',
		'giorni',
		'actions'
	],
	fields: [
		{key: 'nome', label: 'Nome'},
		{key: 'giorni', label: 'Scadenza'},
		{key: 'actions', label: ''},
	],
	buttons: [
		{
			icon: 'trash3',
			eventName: 'deleteEvent',
			disabled: false,
			round: true
		}
	]
};

const eventButtonSchema: Object = {
	buttons: [
		{
			icon: 'plus',
			eventName: 'addEvent',
			round: true,
			filled: true
		}
	]
};

@Component({
	selector: 'dashy-countdown',
	templateUrl: './countdown.component.html',
	styleUrls: ['./countdown.component.scss']
})

export class CountdownComponent implements OnInit {

	tableSchema = eventTableSchema;
	buttonSchema = eventButtonSchema;
	dataSource = eventSource;
	rowSelected;

	constructor(private dialog: MatDialog) {
	}

	ngOnInit() {
		// cosa salvare in localstorage?
		// le scadenze si aggiornano di giorno in giorno?
		// è preferibile che crei un backendino in cui puoi salvare le date e controllare che si aggiornino
	}

	deleteRow($event) {
		console.log($event);
		this.rowSelected = $event.detail;
		this.dataSource.splice(
			this.rowSelected.rowIndex, 1
		);
	}

	openDialog() {
		const dialogConfig = new MatDialogConfig();

		dialogConfig.disableClose = false;
		dialogConfig.autoFocus = true;

		this.dialog.open(EventDialogComponent, dialogConfig);

		const dialogRef = this.dialog.open(EventDialogComponent, dialogConfig);

		dialogRef.afterClosed().subscribe(
			data => {
				this.dataSource.push(
					Object.assign({}, {
						nome: data.description,
						giorni: `- ${data.scadenza} giorni`
					})
				)
			}
		);
	}
}
