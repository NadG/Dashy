import {Component, ElementRef, Input, Renderer2} from '@angular/core';

@Component({
	selector: 'dashy-button-group',
	templateUrl: './button-group.component.html',
	styleUrls: ['./button-group.component.scss']
})
export class ButtonGroupComponent {
	@Input() items: Array<any>;
	@Input() item;

	constructor(private el: ElementRef, private renderer: Renderer2) {
	}

	onParentClick(data) {
		console.log( data.item )
		// this.renderer.listen(this.el.nativeElement, this.items[data.index].eventName , (e) => {
		// 	console.log( data.item);
		// 	console.log(e);
		// 	return data.item
		// })
		this.el.nativeElement.dispatchEvent(new CustomEvent( this.items[data.index].eventName , {
			detail: data.item,
			bubbles: true,
			cancelable: true
		}));
	}
}
