import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
	selector: 'dashy-button',
	templateUrl: './button.component.html',
	styleUrls: ['./button.component.scss']
})
export class ButtonComponent {
	@Input() item?: Object;
	@Input() index: Number;
	@Input() dataSource?: any;
	@Input() disabled?;
	@Input() name?;
	@Input() icon?;
	@Input() rect?: Boolean;
	@Input() round?: Boolean;
	@Input() filled?: Boolean;
	@Input() loneBtn?: Boolean;
	@Output() btnData = new EventEmitter<any>();

	evt() {
		console.log( this.item );
		this.btnData.emit({
			index: this.index,
			item: this.item
		});
	}

}
