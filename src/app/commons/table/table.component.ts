import {Component, Input, OnInit} from '@angular/core';

@Component({
	selector: 'dashy-table',
	templateUrl: './table.component.html',
	styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
	@Input() tableSchema: any;
	@Input() dataSource;
	constructor() {
	}

	ngOnInit() {
	}

}
