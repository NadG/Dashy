# Dashy (˶◕  ‿  ◕˶✿)

Dashboard caruccia e coccolosa per le mie fisse giornaliere:
- il meteo
- chi sta streammando ora su Twitch
- come stanno messi a meta i miei main di LoL
- SPACE! (∩^o^)⊃━☆ﾟ.*･｡ﾟ

## Per Desktop
run  
* electron-packager . --platform=darwin --electron-version=1.6.2 (su mac)
* electron-packager . --platform=win32 (windows)
* ng build --prod && electron . così da creare la cartella dist/ che
 permetterà ad electron di far girare l'app buildata.

Se tutto si sviluppa senza grandi intoppi (e per intoppi intendo: costanza e voglia), in futuro
integrerò anche altre sezioni molto più complesse come:
- un countdown: per avere sempre sott'occhio delle scadenze o capire quanto manca ad un evento bbbbello (♥ω♥*)
- una todo app: mi sono rotta di andare sempre su Keep
- una sorta di app per la dieta
- un ricettario!
- e poi boh! (  • ̀ω•́  )✧

In più la integrerò con electron così ce l'ho sempre sul pc ( e questo presuppone l'imparare electron)

## Bug strani e insensati? 
#### Annotali qui così non perderai più tempo! (ง •̀ω•́)ง✧

**Problemi di CORB o non riesci a creare delle schematics?** Non ti worrare: probabilmente hai recentemente updatato
l'angular-cli globalmente ma ti sei dimenticata di farlo localmente. NP! Runna: `npm install --save-dev @angular/cli@latest` 
e passa la paura! (✿◠ ‿  ◠)

## Note

Ho creato dei mixin che temizzano i componenti in un modo molto rudimentale,
tendenzialmente perchè non ho avuto molto sbatti per studiarmi bene come creare
dei temi con il sass (lo farò un giorno, lo giuro).

Se vuoi temizzare i componenti assicurati che ciascuna view sia wrappata da un 
div id="container" e nel cui foglio scss sia importato @import "~src/assets/sass/styles";.

Puoi creare quanti più temi vuoi. Quello che devi fare è aprire il file mixins.scss e creare un nuovo
mixin come 'theme-amber', importare il webfont che ti piace e poi includere questo mixin
in theming.scss 'silenziando' gli altri temi importati li.

Con questa soluzione potrai cambiare il tema soltanto attraverso questa procedura, e cioè
mettendo mano al file sass interessato. Più avanti farò un sistema con il quale sarà possibile 
switchare tema con un click nel browser (ma non è questo il giorno ( • ̀ω•́  )✧  ).
